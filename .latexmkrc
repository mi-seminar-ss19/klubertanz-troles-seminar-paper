$pdf_mode = 1; # tex -> pdf
@default_files = ('main.tex'); # this the main tex file that includes all the other files
$out_dir = 'build';
$jobname = 'Klubertanz-Troles-Seminar-Paper';

# enable deletion of *.bbl at "latexmk -c"
$bibtex_use = 2;

#remove more files than in the default configuration
@generated_exts = qw(acn acr alg aux code ist fls glg glo gls idx ind lof lot out thm toc tpt);
