# Klubertanz-Troles-Seminar-Paper

The seminar paper for the MI Seminar.

## Deadline

2019-09-30.

## Building

A (full) `LaTeX` distribution must be installed. Clone this project and move to the root folder. Open a terminal and run `latexmk`. You can find the built pdf now in the `build` directory.
