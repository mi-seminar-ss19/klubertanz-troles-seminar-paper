\section{Human psychophysics experiment}\label{chapter:human_psychophysics_experiment}
To test their hypothesis whether humans are also vulnerable to adversarial images that fool CNNs, \cite{elsayed2018adversarial} tested $N=38$ participants in their experiment. Each participant sat in a fixed distance to the monitor and had the task to classify 140 to 950 images as class \emph{A} or \emph{B}. 
As seen in Figure \ref{img:ExperimentalSetup} the participants chose the image class with a response time box whose buttons were randomly assigned to the classes for each experiment to make sure that the positioning or color of the buttons had no effect on responses. 
Each trial consisted of the following five steps:
\begin{enumerate}
	\item A \emph{fixation cross} (500-1000ms)
	\item The \emph{stimulus} (an image of class A or B) (63ms or 71ms)
	\item A \emph{visual masking} consisting of ten random binary masks (200ms)
	\item 2200ms or 2500ms after masking the participants was asked to \emph{categorize} the image as class A or B
	\item \emph{Wait period} of 2200ms or 2500ms till next trial started (regardless of the participants response time)
\end{enumerate}
Before the experiment itself started each participant conducted one or more demo trials with long presentation times and feedback on correctness.


\image{\textwidth}{images/ExperimentalSetup.png}
{Experimental setup and chronology of each trial as shown in \cite{elsayed2018adversarial}.}
{img:ExperimentalSetup}%

\subsection{Experiment conditions}
\cite{elsayed2018adversarial} created four different experiment conditions to test their hypothesis and used three different image groups. Those image groups were: \emph{Pets} (cats and dogs), \emph{Vegetables} (broccoli and cabbage), and \emph{Hazards} (snakes and spiders). Participants worked on only one of the image groups but on each experimental condition. The numbers of trials for each test condition (\emph{adversarial}, \emph{image}, and \emph{flip}) were equally distributed. Those three conditions and the fourth \emph{false-condition} were also balanced considering their distribution of class A and B. 



\begin{itemize}
	\item \textbf{Image condition}: rescaled but otherwise unchanged images from ImageNet were used as a control condition to measure the percentage of correctly classified images.
	\item \textbf{Adversarial condition}: images overlayed with the adversarial perturbation layer with perturbations of a high intensity of $\epsilon=32$ were used. These adversarial images successfully fooled the ten different Machine Learning models to classify images of class A as B and of class B as A. This condition tested the hypothesis, that humans are also fooled by these adversarial images.
	\item \textbf{Flip condition}: the same images and adversarial perturbation layers as in the \emph{adversarial condition} were used, but the adversarial perturbation layer was flipped on a vertical axis. This condition was used to measure the impact of meaningless perturbations in images. These images with flipped perturbations did no longer change the class outcome of the Machine Learning models as the "correct" adversarial images did.
	\item \textbf{False condition}: images from unrelated classes were used (such as airplane or house) and perturbations were added (with a stronger intensity of $\epsilon = 40$) so that the Machine Learning models classified them as class A or B (e.g. an perturbed image of an airplane that was classified as a dog). This condition forced participants to make an incorrect choice because they had to classify an object of class C as class A or class B. This was done to test whether the adversarial perturbations influenced the choice of class towards the one which the adversarial perturbation layer mimics.
\end{itemize}

\cite{elsayed2018adversarial} prefiltered  their dataset for more homogeneous images and to make sure, that the images did not show class specific visually salient features. For example dogs were often shown in front of lawns whereas cats were not. Therefore images with lawns in the background were excluded from the dataset.
Furthermore for a better transfer to humans, strong but class preserving perturbations were used. The class preservation was tested by humans without time-limit, who had to be able to correctly classify the objects shown on the images.

\subsection{Results}
\image{\textwidth}{images/Results.png}
{Bar chart (a) shows the results of the \emph{false condition}. When forced to make a false decision, adversarial perturbations significantly influence the participants choice of class towards the adversarial perturbation class. Bar chart (b) shows the percentage of correct classifications for the \emph{adversarial}, \emph{image}, and \emph{flip condition} (*: p < 0.05; **: p < 0.01; ***: p < 0.001). Graphic from \cite[chap. 4.1]{elsayed2018adversarial}.}
{img:Results}%

The three test conditions \emph{adversarial (adv)}, \emph{image}, and \emph{flip} all resulted in an accuracy far below $1$ due to the very short presentation times. With an accuracy of $\sim0.78$ the \emph{image condition} with the image group \emph{hazard} reached the highest accuracy.
As shown in Figure \ref{img:Results} (b), \cite{elsayed2018adversarial} found that the adversarial perturbations from the \emph{adversarial condition} significantly decreased the accuracy of classification compared to the \emph{image condition} regardless of the image group ($p<0.01$ for the image group \emph{vegetables} and $p<0.001$ for the image groups \emph{pets} and \emph{hazard}). Furthermore the accuracy in the \emph{adversarial condition} was also significantly lower than in the \emph{flip condition} ($p=0.05$ for the image group \emph{hazard} and $p<0.001$ for the image groups \emph{pets} and \emph{vegetables}). 

Figure \ref{img:Results} (a) shows the results of the \emph{false condition}. When forced to choose one of two wrong classes the adversarial perturbations systematically influenced the participants to select the class which was mimicked by the adversarial perturbations. This worked better with the image groups \emph{pets} and \emph{hazard} ($p<0.001$) than with the image group \emph{vegetables} ($p<0.05$).