\subsection{Implications for Machine Learning}\label{chaper:implications_ML}

One aspect that needs to be discussed about the paper from \cite{elsayed2018adversarial} is the resulting impact on machine learning.
Since the paper is still relatively young up to this point wide-ranging consequences are still to be observed, but yet one implication
seems to emerge: \textbf{The development of more robust machine learning models} which has been predicted by the authors from the paper \citep[chap. 5.3]{elsayed2018adversarial}.
One such advancement that builds up on the results from the paper is \cite{menet2019spartan} \textit{\spartannetworks}.
This paper aims to make deep neural networks more resistant to adversarial examples by introducing an \textit{adversarial layer} which is designed to discard some information
with the intention that the network is then forced to focus on the relevant input.

The work of these researchers is motivated by the fact that the vulnerability of neural networks to adversarial inputs brings up another issue:
The models suffer from the so-called \textit{Clever Hans effect} since deep learning algorithms are seeing patterns that are imperceptible to humans under normal conditions.
This term originally comes from a horse that was thought to be capable of solving complex arithmetical operations, but in fact guessed the right answer from the unconscious
behavior of the audience.
This Clever Hans effect is justified by three observations:
\begin{enumerate}
    \item \textit{Sane samples} are almost always well classified by deep learning algorithms.
    \item \textit{Adversarial samples} are likely to be misclassified by deep learning algorithms.
    \item Humans can still classify adversarial samples without a time constraint.
\end{enumerate}
Thus, they deduce that the deep learning algorithms extract features of samples that are not essential to the class of a sample and that those features
do not correspond to the ones humans have learning over the course of their lives. Hence, it is necessary to rework the training phase of deep neural networks since it obviously creates
high sensitivity to adversarial input \citep[chap. 1.1]{menet2019spartan}.

The Spartan Networks, a new deep neural network, is proposed which consists of two conflicting elements forced to collaborate during training:
\begin{enumerate}
    \item \textit{Filtering layers} heavily reduce the amount of information given to the next layer. The \textit{filtering loss} constrains them to output the lowest amount of information.
    \item The remaining connected layers constitute a standard CNN that uses its available information to minimize the \textit{training loss}.
\end{enumerate}
These two parts are antagonists: If the filtering loss is low, i.e. the filtering layer discards all information, the network won't train efficiently, therefore the training loss is high. On
the other hand if the network can train effectively, i.e. reaching a low training loss, the filtering loss will increase.
Hence, the network is called a \textit{self-adversarial} neural network; A weighted sum of those two losses is then formed which should force the filtering layers to find the relevant pieces of information
the other layers of the network need to successfully train against \citep[chap. 1.2]{menet2019spartan}.

Since excessive linearity seems to be a vulnerable point of neural networks, the problem of current deep neural networks are the very large input space for
a small output space and the linearity makes it possible for the attacker to explore this space to find adversarial examples \citep[chap. 5.1]{menet2019spartan}, as illustrated in \cref{img:dnn_problems}.
\image{.6\textwidth}{images/DNN-problems.png}{
    A DNN with the locally-linear activation function ReLU \citep[chap. 5.2]{menet2019spartan}.
    If such a function is used, an attacker can find weights in order to manipulate the prediction towards misclassification.
    Red means an increase, Blue a decrease, in the value of a part of the input respectively.
}{img:dnn_problems}
Thus, the robust learning in this paper is founded on the following aspects \citep[chap. 5.2]{menet2019spartan}:
\begin{itemize}
    \item Non-linear behavior
    \item Reduction of the attack space by \textit{feature squeezing}
    \item Binary activation values instead of float values
\end{itemize}
\textit{Feature Squeezing} is a special defense strategy against adversarial examples which is also driven by the observation that feature input spaces are often
unnecessarily large and therefore providing vast opportunities for adversarial examples to work. The term \enquote{squeezing} refers to the concept of squeezing out
unnecessary input features to reduce the available degrees of freedom. The underlying idea is to compare the model's prediction on the original sample with the one
after squeezing: If the two produce significantly different outputs, the input is recognized to be probably an adversarial \citep[chap. 1]{xu2017feature}, see \cref{img:feature_squeezing}.
\image{\textwidth}{images/feature-squeezing.png}{
    \textit{The feature-squeezing framework} \citep[chap. 1]{xu2017feature}.
    If the difference between both predictions exceeds a certain threshold, the input is identified to be an adversarial.
}{img:feature_squeezing}
\image{.5\textwidth}{images/spartan-network.png}{
    \textit{The Spartan Network} \citep[chap. 5.3]{menet2019spartan}.
    The first (marked) layer squeezes the features of the input and outputs only binary values.
    $L1$ is the global loss of the network to force this layer to output as few as possible.
    This process is called \textit{data-starving}.
}{img:spartan_network}
So by using semantic-preserving compression or filtering (e.g. going from grayscale to black and white values) one can detect adversarial samples using the fact that
sane samples often create agreeing predictions on different compressions in contrast to adversarial samples whose predictions are different \citep[chap. 4.3]{menet2019spartan}.
The specific structure of a \textit{Spartan Network} is depicted in \cref{img:spartan_network}.
For the full details about the filtering layers refer to \cite[chap. 6]{menet2019spartan}.

\image{.8\textwidth}{images/FGSM-attack.png}{
	Comparison of a Spartan Network vs. a vanilla CNN in a FGSM attack \citep[chap. 7]{xu2017feature}.
	Generally, Spartan Networks are more robust but less precise.
}{img:fgsm_attack}

However, interesting are the results of the evaluation of Spartan Networks since one of the biggest implications is
an inherent \textit{robustness-precision tradeoff}. For more robustness against adversarials we get less precision on sane samples, consequently
an application of such networks is more reasonable when dealing with a lot of adversarial examples.
Specific numbers about the precision and robustness are present if the potential attack is done using a Fast Gradient Signed Method (FGSM)
with an epsilon value of $\epsilon = 0.3$. The Spartan Network instance is in this case more robust to FGSM by 20\%, but about 0.5\% less precise
on sane samples \citep[chap. 8]{menet2019spartan}, \cref{img:fgsm_attack} shows results with a varying epsilon value.

