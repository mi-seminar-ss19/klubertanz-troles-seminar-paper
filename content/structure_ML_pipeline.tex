\section{Structure of Machine Learning pipeline}\label{chapter:structure_ml_pipeline}

To arrive at the point where we have adversarial examples that should also fool humans, the researchers from
the \cite{elsayed2018adversarial} paper pass through three distinct steps that can be visualized in pipeline form (see \cref{img:ML_pipeline_overview}).
%
\image{0.9\textwidth}{images/ML_pipeline.png}{Overview of ML vision pipeline}{img:ML_pipeline_overview}%
%

\subsection{Data set of images}

For the underlying data base of the images the popular image database ImageNet has been used.
As every image has a specifically assigned class, six coarse classes for further processing have been selected:
\textit{Dog, cat, broccoli, cabbage, spider, snake}. The premise is here that all participants in the later conducted experiment should be familiar
with identifying instances from the aforementioned classes.
Subsequently these six classes were generalized into the following groups \citep[chap. 3.1.1]{elsayed2018adversarial}:
\begin{itemize}
    \item \textbf{Pets} (dogs and cats)
    \item \textbf{Hazard} (spiders and snakes)
    \item \textbf{Vegetables} (broccoli and cabbage)
\end{itemize}

\subsection{Group of CNN models with retinal layer}\label{chapter:CNNModelsRetinalLayer}

The next step in the pipeline is to have a group or ensemble of CNN models which should be fooled by the generated
adversarial images that eventually should transfer to human beings.

Hence, overall ten models were trained on the ImageNet data set which are instances of different architectures.
However, each model is prepended with a special property: A \textbf{retinal layer} which artificially limits each model in its information extraction.
More specifically, the retinal layer is supposed to match the initial processing of the human visual system, i.e. to approximate the input received by
the visual cortex of humans through their retinal lattice. For this matter an \textit{eccentricity dependent blurring} is performed which determines
and applies the degree of spatial blurring at each image location \citep[chap. 3.1.2]{elsayed2018adversarial}.
The reason for the existence of this retinal layer is discussed in \cref{chapter:relevance_retinal_layer}.

\subsection{Generation of adversarial images}

The last step in the pipeline is the actual generation of the adversarial images that are supposed to trick the models into making a mistake.
A major constraint is here that those adversarial images \textbf{strongly transfer} across the whole group of models -- motivated by the thought
that this transferability correlates with the transfer to humans.

Adversarial images are expressed like this: For a class tuple $(A, B)$ generate adversarial perturbations such that models will classify perturbed
images from $A$ as $B$; The perturbations differ in each image, but the norm of all perturbations $l_\infty$ shall be constrained to be equal to a
fixed $\epsilon$. Formally: There is a classifier, which assigns probability $P(y \mid X)$ to each coarse class $y$ given an input image $X$,
the specified adversarial target class $y_{target}$, and the perturbation magnitude $\epsilon$. The goal is to find the image $X_{adv}$ which \textit{minimizes}
$-\log(P(y_{target} \mid X_{adv}))$ with the constraint that $||X_{adv} - X||_{\infty} = \epsilon$.
The classifier's parameters are then used to perform \textit{iterated gradient descent} on $X$ to generate the $X_{adv}$ \citep[chap. 3.1.3]{elsayed2018adversarial}.
Additionally, to encourage a high transfer rate across the group of models, only those adversarial images are retained that fool the majority of
CNNs at the same time \citep[appx. D]{elsayed2018adversarial}.

% todo add graphics with listed CNN architectures and success rates
\image{0.8\textwidth}{images/table_attack_success_model_ensemble.png}
{Attack success on group of CNN models on images at adversarial conditions \textit{adv} and \textit{flip} \citep[appx. A]{elsayed2018adversarial}.
The number triplets represent accuracy on images from groups pets, hazard, vegetables, respectively. The aforementioned conditions are explained later
in the dedicated chapter about the experiment.}
{img:table_attack_success_model_ensemble}%

As \cref{img:table_attack_success_model_ensemble} shows both the different architectures used in the CNN model group and the attack success of the
generated adversarial on the model ensemble, it is clear why the success percentage is throughout always $100\%$ for each model; Because, as mentioned above,
the adversarial images are only retained if they fool the majority of CNNs, i.e. only if all models are fooled at the same time in the \textit{adv} condition and if at least $7/10$ models are fooled
in the \textit{false} condition \citep[appx. D]{elsayed2018adversarial}.

\image{0.8\textwidth}{images/table_attack_success_test_models.png}
{Attack success on test models, trained on both clean and adversarial images \citep[appx. A]{elsayed2018adversarial}. Number triplets are the same as in \cref{img:table_attack_success_model_ensemble}.}
{img:table_attack_success_test_models}%

\Cref{img:table_attack_success_test_models} on the other hand shows the attack success of the adversarial images on test models, to show the strong transferability of the images across models.
It can be seen that the adversarial images still work in more than the majority of images provided to the test models.
